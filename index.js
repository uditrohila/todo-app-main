let form = document.querySelector("form");
let text = document.getElementById("todo-input");
let todoitems = document.querySelector(".todo-items");
let todoitem = document.querySelector(".todo-item");
let itemsLeft = document.querySelector('.items-left span');
form.addEventListener('submit', (e) => {
    e.preventDefault();
    // console.log(text.value);
    addtodo();
})

function addtodo(){
    let todocoll = document.createElement("div");
    todocoll.classList.add("todocoll");
    let todotext = text.value;
    
    todocoll.innerHTML = `<div class="todo-item">
    <!-- check button container -->
    <div class="check">
      <div class="check-mark">
        <img src="./images/icon-check.svg" />
      </div>
    </div>
    <!-- todo items display -->
    <div class="todo-text">${todotext}</div>
    <button class="close"><img src="./images/icon-cross.svg" alt=""></button>
  </div>`

  todoitems.appendChild(todocoll);
  let todonewitem = document.querySelector(".todocoll");
  // update the item count
  let totalitems = document.querySelectorAll(".todo-items");
  updateItemsCount(totalitems.length);

  
  

  //do the active check part
  let check = todocoll.querySelector(".todo-item .check .check-mark");
  check.addEventListener('click', () => {
  check.classList.toggle("active-check");   
 // console.log(check);
 // todocoll.children[1].classList.add("complete");
 // console.log(todocoll.children[0].children[1]);
 todocoll.children[0].children[1].classList.toggle("complete");

})



//remove todo
let close = todocoll.querySelector(".close");
close.addEventListener("click", ()=>{
    todocoll.remove();
})



}




function updateItemsCount(number){
  itemsLeft.innerText =  +itemsLeft.innerText + number;
}




//  clear completed logic
let clear = document.querySelector(".items-clear");
clear.addEventListener("click", ()=>{
    let todoli = document.querySelectorAll(".todocoll");
    todoli.forEach(elem => {
        if(elem.children[0].children[1].classList.contains("complete")){
            elem.remove();
        }
      
    });
})





// filters logic
let statusinfo = document.querySelectorAll(".items-statuses span");
// console.log(statusinfo);
// console.log(todoli);

statusinfo.forEach(elem => {
    elem.addEventListener("click", () => {
        statusinfo.forEach(item => {
            item.classList.remove("active");
        });
    elem.classList.add("active");
   let todoli = document.querySelectorAll(".todocoll");
    if(elem.innerText == 'Active'){
         todoli.forEach(elem => {
             if(!elem.children[0].children[1].classList.contains("complete")){
                 elem.style.display="block";
             }
             else
             {
                 elem.style.display="none";
             }
         })
    }

    else if(elem.innerText == "Completed"){
        todoli.forEach(elem => {
            if(elem.children[0].children[1].classList.contains("complete")){
                elem.style.display="block";
            }
            else
            {
                elem.style.display="none";
            }
        })
    }

    else
    {
       todoli.forEach(elem => {
           elem.style.display="block";
       })
    }

    })
});



